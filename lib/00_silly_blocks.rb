def reverser
  yield.split.each {|word| word.reverse!}.join(' ')
end

def adder(n = 1)
  n + yield
end

def repeater(number = 1)
  number.times{yield}
end
